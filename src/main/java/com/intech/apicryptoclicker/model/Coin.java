package com.intech.apicryptoclicker.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Coin {
    @Id
    public int id;

    public int Bitcoin;

    public int Euro;

    public void setId(int id) {
        this.id = id;
    }

    public int setBitcoin(int Bitcoin) {
        this.Bitcoin = Bitcoin;
        return Bitcoin;
    }

    public int setEuro(int Euro) {
        this.Euro = Euro;
        return Euro;
    }
}
