package com.intech.apicryptoclicker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.awt.event.InputEvent;

@SpringBootApplication
public class ApicryptoclickerApplication {

	public static void main(String[] args) {

		SpringApplication.run(ApicryptoclickerApplication.class, args);
		System.out.println("Cryptoclicker");
	}

}
