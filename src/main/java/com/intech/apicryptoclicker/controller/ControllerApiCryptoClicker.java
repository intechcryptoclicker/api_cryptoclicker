package com.intech.apicryptoclicker.controller;


import com.intech.apicryptoclicker.model.Coin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crypto-server")
class ControllerApiCryptoClicker {
    // to do

    @GetMapping("/")
    public String helloWorld() {

        return "Hello World!";

    }

    @GetMapping("/coin")
    public Coin getARandomCoin() {
        Coin n = new Coin();
        n.setId(0);
        return n;
    }

    @GetMapping("/Bitcoin")
    public Coin getBitcoin(){
        Coin m = new Coin();
        m.setBitcoin(1);
        return m;
    }

    @GetMapping("/Euro")
    public Coin getEuro(){
        Coin e = new Coin();
        e.setEuro(58750);
        return e;
    }

}
